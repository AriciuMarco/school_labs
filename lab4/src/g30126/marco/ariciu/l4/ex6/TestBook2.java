package g30126.marco.ariciu.l4.ex6;


import g30126.marco.ariciu.l4.ex4.Author;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBook2 {
    @Test
    public void testToString()
    {
        Author[] a=new Author[2];
        a[0]=new Author("a0","a0@",'m');
        a[1]=new Author("a1","a1@",'f');
        Book book=new Book("b1", a, 10);
        assertEquals("b1by2authors",book.toString());

    }
}
