package g30126.marco.ariciu.l4.ex5;


import g30126.marco.ariciu.l4.ex4.Author;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBook {
    @Test
    public void testtoString() {
        Author a1=new Author("mihai", "lalal", 'M');
        Book b=new Book("carte",a1,10);
        assertEquals(b.getName()+"by"+a1.toString(),b.toString());
    }

}