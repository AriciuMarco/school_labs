package g30126.marco.ariciu.l4.ex7;

import static junit.framework.Assert.assertEquals;


import org.junit.Test;

public class TestCylinder {
    @Test
    public void testGetVolume()
    {
        Cylinder cylinder=new Cylinder(10, 10);
        assertEquals( Math.PI*cylinder.getRadius()*cylinder.getHeight(), cylinder.getVolume());
    }
}