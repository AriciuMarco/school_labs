package g30126.marco.ariciu.l4.ex3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

class Testcircle {
    @Test
    public void testGetArea() {
        Circle c=new Circle();
        assertEquals(Math.PI*c.getRadius(), c.getArea(),0.01);
    }

}
