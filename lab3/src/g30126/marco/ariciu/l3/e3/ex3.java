package g30126.marco.ariciu.l3.e3;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;

public class ex3 {
        public static void main(String[] args)
        {
            // Set up the initial situation
            City prague = new City();

            Robot karel = new Robot(prague, 5, 1, Direction.NORTH);

            // Direct the robot to the final situation
            karel.move();
            karel.move();
            karel.move();
            karel.move();
            karel.move();
            karel.turnLeft();	// start turning right as three turn lefts
            karel.turnLeft();
            karel.move();
            karel.move();
            karel.move();
            karel.move();
            karel.move();

            karel.move();
        }
    }

