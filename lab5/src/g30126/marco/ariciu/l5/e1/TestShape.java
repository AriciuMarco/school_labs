package g30126.marco.ariciu.l5.e1;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestShape {
    @Test
    public void testShape() {

        Shape[] shapes=new Shape[3];
        shapes[0]=new Circle("red",true,2.5);
        shapes[1]=new Rectangle(2,5,"blue",false);
        shapes[2]=new Square(4,"red",true);
    }
}
