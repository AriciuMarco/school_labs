package g30126.marco.ariciu.l5.e2;

public class ProxyImage implements Image {

    private Image image;
    private String fileName,type;

    public ProxyImage(String fileName, String type){
        this.fileName = fileName;
        if ("real".equals(type)) {
            image = new RealImage(fileName);

        } else if ("rotated".equals(type)) {
            image = new RotatedImage(fileName);

        }
    }

    @Override
    public void display() {
        image.display();
    }
    public static void main(String[] args) {
        ProxyImage proxyImage=new ProxyImage("mere","rotated");
        proxyImage.display();
    }
}

