package g30126.marco.ariciu.l6.e3;

import javax.swing.*;
import java.awt.*;
import java.awt.Shape;


public class DrawingBoard  extends JFrame {

    java.awt.Shape[] shapes = new java.awt.Shape[10];
    //ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }
    public void deleteById (String id) {
        for(int i=0;i<shapes.length;i++){
            if(shapes[i].getId().equals(id))
            {
                shapes[i]=null;
            }
            repaint();
        }

    }



    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }
}