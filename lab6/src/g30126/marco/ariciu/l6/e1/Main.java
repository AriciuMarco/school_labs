package g30126.marco.ariciu.l6.e1;

import java.awt.*;
import java.awt.Rectangle;
import java.awt.Shape;

public class Main {

    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        java.awt.Shape s1 = (Shape) new Circle(Color.RED,"1",10,50,true, 90);
        b1.addShape(s1);
        java.awt.Shape s2 = (Shape) new Circle(Color.GREEN,"2",300, 50,true, 100);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLACK,"3",400,400,true,100,200);
        b1.addShape(s3);
        b1.deleteShape("2");
    }
}